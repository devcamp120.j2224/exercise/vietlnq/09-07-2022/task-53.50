import java.util.ArrayList;

import com.devcamp.j04_javabasic.s10.CBird;
import com.devcamp.j04_javabasic.s10.CCat;
import com.devcamp.j04_javabasic.s10.CDog;
import com.devcamp.j04_javabasic.s10.CFish;
import com.devcamp.j04_javabasic.s10.CPerson;
import com.devcamp.j04_javabasic.s10.CPet;

public class App {
    public static void main(String[] args) throws Exception {
        /* System.out.println("Subtask 5 đây thầy ơi:");
        CPerson subtask5 = new CPerson();

        ArrayList<CPet> petList = new ArrayList<>();
        CPet conMeo = new CCat();
        CPet conCho = new CDog();
        petList.add(conCho);
        petList.add(conMeo);
        subtask5.setPets(petList);
        System.out.println(subtask5.toString());

        // Subtask 6:
        subtask5.setFirstName("Subtask 6 đây thầy ơi!");
        System.out.println(subtask5.getFirstName()); */

        /************* Task 53.50 subtask 5: ****************/
        CPerson person1 = new CPerson();
        
        CPet conMeo1 = new CCat();
        CPet conCho1 = new CDog();
        CPet conCa1 = new CFish();
        CPet conChim1 = new CBird();

        ArrayList<CPet> petList1 = new ArrayList<>();
        
        petList1.add(conMeo1);
        petList1.add(conCho1);
        petList1.add(conCa1);
        petList1.add(conChim1);

        person1.setPets(petList1);

        System.out.println(person1.getPets().get(0));
    }
}
